<?php

require_once "database.php";
$LMI_PAYEE_PURSE = "14947";
$LMI_PAYMENT_AMOUNT = "1.00";
$LMI_PAYMENT_DESC = "testing";
$LMI_PAYMENT_NO = "1";
$CLIENT_MAIL = "";
$ZP_SIGN = "";
$ZP_CODE_OPPER="";

$URL = "https://z-payment.com/merchant.php";
$POST_OR_GET = 'POST';
$result_answer = "";

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset ($_POST['LMI_PAYEE_PURSE'])) $LMI_PAYEE_PURSE = $_POST['LMI_PAYEE_PURSE'];
    if (isset ($_POST['LMI_PAYMENT_AMOUNT']) && $_POST['LMI_PAYMENT_AMOUNT']>0) $LMI_PAYMENT_AMOUNT = $_POST['LMI_PAYMENT_AMOUNT'];
    if (isset ($_POST['LMI_PAYMENT_NO']) && $_POST['LMI_PAYMENT_NO'] !== "" ) $LMI_PAYMENT_NO = $_POST['LMI_PAYMENT_NO'];
        else $LMI_PAYMENT_NO = getId() + 1;
    if (isset ($_POST['CLIENT_MAIL'])) $CLIENT_MAIL= $_POST['CLIENT_MAIL'];
    if (isset ($_POST['ZP_SIGN'])) $ZP_SIGN = $_POST['ZP_SIGN'];
    if (isset ($_POST['ZP_CODE_OPER'])) $ZP_CODE_OPER = $_POST['ZP_CODE_OPER'];
    if (isset ($_POST['LMI_PAYMENT_DESC'])) $LMI_PAYMENT_DESC = $_POST['LMI_PAYMENT_DESC'];

    if (savePay($LMI_PAYMENT_NO, $CLIENT_MAIL)) {
        $values = array(
            "LMI_PAYEE_PURSE" => $LMI_PAYEE_PURSE,
            "LMI_PAYMENT_AMOUNT" => $LMI_PAYMENT_AMOUNT,
            "LMI_PAYMENT_DESC"=>$LMI_PAYMENT_DESC,
            "LMI_PAYMENT_NO" => $LMI_PAYMENT_NO,
            "CLIENT_MAIL" => $CLIENT_MAIL,
        );

        $values1 = array();
        foreach ($values as $key => $value) {
            $values1 []= "$key=$value";
        }
        $request = implode('&',$values1);
        echo($request);
        /*$response = new HttpResponse();
        $response->setData($request);
        $response->status(200);
        $response->redirect($URL);*/
        $rd_rul = "$URL?$request";
        header('Location: '. $rd_rul, true, 301);
        die();
    } else echo "not saved";

}

function savePay($payment_no, $email) {
    try{
        $dbh = new PDO('mysql:host='.$GLOBALS['db_host'].';dbname='.$GLOBALS['db_name'], $GLOBALS['db_user'], $GLOBALS['db_password']);
        $sql = "INSERT INTO operations (id_transaction, email, status_check, status) VALUES (?, ?, 0, 1)";
        $sth = $dbh->prepare($sql);
        if ($sth->execute(array($payment_no, $email))) {
            $result = true;
            $row = $sth->fetchAll();
            //var_dump($row);
        } else $result = false;
    } catch (PDOException $e) {
        echo "Error!: " . $e->getMessage() . "<br/>";
        return null;
    }

    $dbh = null;
    return $result;
}

function getId() {
    try{
        $dbh = new PDO('mysql:host='.$GLOBALS['db_host'].';dbname='.$GLOBALS['db_name'], $GLOBALS['db_user'], $GLOBALS['db_password']);
        $sql = "SELECT id_transaction FROM operations";
        $sth = $dbh->prepare($sql);
        if ($sth->execute()) {
            $rows = $sth->fetchAll();
            $result = array_pop($rows);
            $result = $result['id_transaction'];
            //var_dump($rows);
        } else $result = 0;
    } catch (PDOException $e) {
        echo "Error!: " . $e->getMessage() . "<br/>";
        return 0;
    }

    $dbh = null;
    return $result;
}

function SendRequest($ACT_URL, $Values, $POST_OR_GET='POST') {
    //Если метод передачи GET
    if($POST_OR_GET=='GET') {
        $Result = file($ACT_URL.'?'.$Values);
        if($Result===false) {
            die("Не удалось получить ответ по запросу");
        }
        $Result = $Result[0];

    }
    //Если метод передачи POST
    elseif($POST_OR_GET=='POST') {
        /////////////////////////////////////////////////////
        //Для работы примера требуется установленный модуль//
        //библиотеки CURL cайт пакета http://curl.haxx.se  //
        /////////////////////////////////////////////////////
        //Инициализация модуля CURL
        $ch = curl_init($ACT_URL);
        if(!$ch) {
            die('Инициализация модуля CURL не выполнена, проверьте его установку');
        }
        //Запрос POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //Задаем переменные
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Values);
        //HTTP заголовки не выводить
        curl_setopt($ch, CURLOPT_HEADER, 0);
        //Требуется получить результат запроса
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //Редирект запретить
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        //Выполнение запроса
        $Result = curl_exec($ch);
        if($Result===false) {
            die("Error CURL number: " .curl_errno($ch)." message: ".curl_error($ch));
        }
        curl_close($ch);
    }
    return $Result;
}