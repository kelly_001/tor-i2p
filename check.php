<?php
include_once "database.php";

$STATUS_CREATED = 0;
$STATUS_WAIT_PAY = 1;
$STATUS_WAIT_PROCESSING = 2;
$STATUS_PROCESSING = 3;
$STATUS_FINISHED = 4;
$STATUS_CANCELLED = 5;
$STATUS_FROZEN = 6;
$STATUS_ERROR = 99;

$STATUS_CHECKED = 2;
$STATUS_PAID = 5;
$STATUS_NEW = 0;
$STATUS_NEED_CHECK = 1;
//id магазина из личного кабинета z-payment
$ID_SHOP = "14947";
//merchant key  из личного кабинета z-payment
$MERCHANT_KEY = "7fOfZSSRBoyOhiWS5hsI7BzdRguckr";
$POST_OR_GET = 'GET';
$ACT_URL = 'https://z-payment.com/api/get_status_pay.php';

$dbh = new PDO('mysql:host=' . $GLOBALS['db_host'] . ';dbname=' . $GLOBALS['db_name'], $GLOBALS['db_user'], $GLOBALS['db_password']);
$query = 'SELECT id_transaction FROM operations WHERE status_check=0 OR status_check = 1';
try {
    $result = $dbh->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_SCROLL));
    $result->execute();
    while ($row = $result->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
        $ID_INVOICE = $row['id_transaction'];
        $HASH = md5($ID_SHOP . $ID_INVOICE . $MERCHANT_KEY);
        $Values = "ID_SHOP=$ID_SHOP&ID_INVOICE=$ID_INVOICE&HASH=$HASH";
        $Result = SendRequest($ACT_URL, $Values, $POST_OR_GET);
        $Data = explode('&', $Result);
        $ArValue = array();
        foreach ($Data as $Value) {
            $PartValue = explode('=', $Value);
            $ArValue[$PartValue[0]] = $PartValue[1];
            echo 'ArValue[' . $PartValue[0] . ']=' . $ArValue[$PartValue[0]] . '<br>';
            if ($PartValue[0] == 'STATUS')
            {
                //update db
                if ($PartValue[1] == $STATUS_FINISHED || $PartValue[1] == $STATUS_FROZEN) {
                    $query = 'UPDATE operations SET status_check=?, status =? WHERE id_transaction = ?';
                    $query_result = $dbh->prepare($query);
                    $query_result->execute(array($STATUS_PAID, $PartValue[1], $ID_INVOICE));
                    $query_result = null;
                } else if ($PartValue[1] <= 6) {
                    $query = 'UPDATE operations SET status_check=?, status =? WHERE id_transaction = ?';
                    $query_result = $dbh->prepare($query);
                    $query_result->execute(array($STATUS_NEED_CHECK, $PartValue[1], $ID_INVOICE));
                    $query_result = null;
                } else
                {
                    $query = 'UPDATE operations SET status_check=?, status =? WHERE id_transaction = ?';
                    $query_result = $dbh->prepare($query);
                    $query_result->execute(array($STATUS_CHECKED, $PartValue[1], $ID_INVOICE));
                    $query_result = null;
                }
            }
        }


    }
    $dbh = null;
} catch (PDOException $e) {
    print $e->getMessage();
}

function SendRequest($ACT_URL, $Values, $POST_OR_GET = 'POST')
{
    //Если метод передачи GET
    if ($POST_OR_GET == 'GET') {
        $Result = file($ACT_URL . '?' . $Values);
        if ($Result === false) {
            die("Не удалось получить ответ по запросу");
        }
        $Result = $Result[0];

    } //Если метод передачи POST
    elseif ($POST_OR_GET == 'POST') {
        /////////////////////////////////////////////////////
        //Для работы примера требуется установленный модуль//
        //библиотеки CURL cайт пакета http://curl.haxx.se  //
        /////////////////////////////////////////////////////
        //Инициализация модуля CURL
        $ch = curl_init($ACT_URL);
        if (!$ch) {
            die('Инициализация модуля CURL не выполнена, проверьте его установку');
        }
        //Запрос POST
        curl_setopt($ch, CURLOPT_POST, 1);
        //Задаем переменные
        curl_setopt($ch, CURLOPT_POSTFIELDS, $Values);
        //HTTP заголовки не выводить
        curl_setopt($ch, CURLOPT_HEADER, 0);
        //Требуется получить результат запроса
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //Редирект запретить
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        //Выполнение запроса
        $Result = curl_exec($ch);
        if ($Result === false) {
            die("Error CURL number: " . curl_errno($ch) . " message: " . curl_error($ch));
        }
        curl_close($ch);
    }
    return $Result;
}

?>